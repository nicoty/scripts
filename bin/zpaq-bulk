#!/bin/sh
# dependencies: `lib.sh`, `zpaq`

main () { (
# -e = exit if any statement returns a non-true return value. -u = exit if an attempt is made to use an uninitialised variable.
  set -eu &&

  sh_lib="${HOME}/.local/lib/lib.sh"

# shellcheck source=../lib/lib.sh
  . "${sh_lib}" &&

# make the remove list for trap
  list_rm="$(mktemp)"

# set trap
  trap 'notify_exit_early "${list_rm}"' HUP INT QUIT ABRT ALRM TERM &&

# set variables
  file_self="$(basename "$(readlink -f "${0}")")"
  version_self="0.1.0"
# will_exit used to allow other options to be parsed too
  will_exit=1
  boolean_add=1
  boolean_process_contents=1

# define internal functions
  print_help () {
    printf 'Run `zpaq` with appropriate options. By
default, the contents of the current
directory are processed. If no
arguments are passed, the script will
attempt to extract any zpaq archives
in the current directory. If `--add`
flag is used, zpaq archives of contents
of the current directory are
created/updated. If directories are
passed as arguments and the
`--process-contents` flag is used, any
contents will be individually
extracted/archived (depending on the
presence of the `--add` flag),
otherwise if files or directories are
passed without `--process-contents`
flag, these are archived/extracted
instead (again, depending on the
presence of the `--add` flag).

USAGE:
  %s [FLAGS] [OPTIONS] (--
  [DIRECTORIES] [FILES]
  ["MULTI-PART ARCHIVES*?.zpaq"] ...)

FLAGS:
  -a, --add
    Create or update (a) zpaq
    archive(s).

  -c, --process-contents
    Extract any zpaq archives contained
    in input directories. If used with
    the `--add` flag, create or update
    zpaq archives of any contents of
    input directories.

  -d, --debug
    Print commands and their arguments
    as they are executed.

  -h, --help
    Print this message then exit.

  -V, --version
    Print version information then
    exit.

OPTIONS:
  -k, --key <KEY>
    Encrypt/decrypt zpaq archives using
    KEY provided.

  -m, --method <METHOD>
    Used with `--add` flag. Compress
    using METHOD provided. See zpaq(1)
    for details.

  -o, --output <OUTPUT>
    Write outputs to OUTPUT directory
    provided. Defaults to current
    directory.

  -s, --split <SIZE>
    Used with `--add` flag. Split
    outputs into parts of size SIZE
    bytes.

EXAMPLES:
  Make archives of the contents of the
  current directory, split them into
  100MB parts, output into the
  current directory:
    %s --add --process-contents --split
    100000000 -- "./"

  Extract the multi-part archive prefixed
  "NAME" into the current directory:
    %s --output "./" -- "NAME*?.zpaq"
' "${file_self}" "${file_self}" "${file_self}" >&2
  }

  add_single () { (
    for argument in "${@}" ; do
      optarg="${argument%%=*}"
      if test "${optarg}" = "input" || test "${optarg}" = "i" ; then
        input="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "output" || test "${optarg}" = "o" ; then
#       option is output or o and length of value is nonzero, so try to use value as output
        output="${argument#*=}"
      else
#       set default output destination
        output="${input}.zpaq"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "key" || test "${optarg}" = "k" ; then
#       option is key or k and length of value is nonzero, so try to use value as key
        key="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "method" || test "${optarg}" = "m" ; then
#       option is method or m and length of value is nonzero, so try to use value as method
        method="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "split" || test "${optarg}" = "s" ; then
#       option is split or s and length of value is nonzero, so try to split output using value as size
        split="${argument#*=}"
      fi
    done &&

    if test "${input:+set}" != "set" ; then
      error "Parameter \"input\" in function \"add_single\" has not been set or has null value.\n" "2" >&2
    fi &&

    if test "${split:+set}" = "set" ; then
#     --block-size and --output options are not POSIX compliant. todo: use POSIX compliant alternatives
      df_tmp="$(df --block-size=1 --output=avail /tmp)"
      tmp_free="${df_tmp##* }"
#     this is done to account for unexpected decrease of available space in /tmp.
      tmp_free_conservative="$(printf '%s * 0.99\n' "${tmp_free}" | bc --mathlib)"
      du_input="$(du --summarize --bytes "${input}")"
      file_size="${du_input%%	*}"
#     check if 99% of free space of /tmp is greater than size of input
      if test "${tmp_free_conservative%%.*}" -gt "${file_size}" ; then
#       it is, so use /tmp for processing
        file_temporary="$(mktemp --dry-run)"
      else
#       it isn't, so use "${output}.tmp" instead
        file_temporary="${input}.tmp"
      fi &&
      printf '%s\n' "${file_temporary}" >> "${list_rm}" &&
      set -- "add" "${file_temporary}" "${input}" "-to" "./${input}"
    else
      set -- "add" "${output}" "${input}" "-to" "./${input}"
    fi &&
    if test "${key:+set}" = "set" ; then
      set -- "${@}" "-key" "${key}"
    fi &&
    if test "${method:+set}" = "set" ; then
      set -- "${@}" "-method" "${method}"
    fi &&
    if test "${split:+set}" = "set" ; then
      zpaq "${@}" &&
#     + 1 added for rounding purposes. "${split}" needs to be in bytes and have no units. todo: parse units
      fragment_amount="$(printf '(%s / %s) + 1\n' "${file_size}" "${split}" | bc --mathlib)"
      fragment_amount_integer_part="${fragment_amount%%.*}"
#     the # in "${#fragment_amount_integer_part} counts its length
      split --suffix-length="${#fragment_amount_integer_part}" --additional-suffix=".zpaq" --bytes="${split}" --numeric-suffixes="1" "${file_temporary}" "${input}" &&
      rm "${file_temporary}" "${list_rm}"
    else
      zpaq "${@}"
    fi
  ) }

  add_contents () { (
    for argument in "${@}" ; do
      optarg="${argument%%=*}"
      if test "${optarg}" = "input" || test "${optarg}" = "i" ; then
        input="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "output" || test "${optarg}" = "o" ; then
#       option is output or o and length of value is nonzero, so try to use value as output
        output="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "key" || test "${optarg}" = "k" ; then
#       option is key or k and length of value is nonzero, so try to use value as key
        key="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "method" || test "${optarg}" = "m" ; then
#       option is method or m and length of value is nonzero, so try to use value as method
        method="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "split" || test "${optarg}" = "s" ; then
#       option is split or s and length of value is nonzero, so try to split output using value as size
        split="${argument#*=}"
      fi
    done &&

    if test "${input:+set}" != "set" ; then
      error "Parameter \"input\" in function \"add_contents\" has not been set or has null value.\n" "2" >&2
    fi &&

    for content in "${input}/"?* ; do
      set -- "input=${content}" &&
      if test "${output:+set}" = "set" ; then
        set -- "${@}" "output=${output}"
      fi &&
      if test "${key:+set}" = "set" ; then
        set -- "${@}" "key=${key}"
      fi &&
      if test "${method:+set}" = "set" ; then
        set -- "${@}" "method=${method}"
      fi &&
      if test "${split:+set}" = "set" ; then
        set -- "${@}" "split=${split}"
      fi &&
      add_single "${@}"
    done
  ) }

  extract_single () { (
    for argument in "${@}" ; do
      optarg="${argument%%=*}"
      if test "${optarg}" = "input" || test "${optarg}" = "i" ; then
        input="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "key" || test "${optarg}" = "k" ; then
#       option is key or k and length of value is nonzero, so try to use value as key
        key="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "output" || test "${optarg}" = "o" ; then
#       option is output or o and length of value is nonzero, so try to use value as output
        output="${argument#*=}"
      fi
    done &&

    if test "${input:+set}" != "set" ; then
      error "Parameter \"input\" in function \"extract_single\" has not been set or has null value.\n" "2" >&2
    fi &&

    set -- "extract" "${input}" &&
    if test "${key:+set}" = "set" ; then
      set -- "${@}" "-key" "${key}"
    fi &&
    if test "${output:+set}" = "set" ; then
      set -- "${@}" "-to" "${output}"
    fi &&
#   todo: curretly, the script exits if zpaq can't extract successfully. find a way to keep processing other files even after an unsuccessful attempt.
    zpaq "${@}"
  ) }

  extract_contents () { (
    for argument in "${@}" ; do
      optarg="${argument%%=*}"
      if test "${optarg}" = "input" || test "${optarg}" = "i" ; then
        input="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "key" || test "${optarg}" = "k" ; then
#       option is key or k and length of value is nonzero, so try to use value as key
        key="${argument#*=}"
      fi &&

      if test -n "${argument#*=}" && test "${optarg}" = "output" || test "${optarg}" = "o" ; then
#       option is output or o and length of value is nonzero, so try to use value as output
        output="${argument#*=}"
      fi
    done &&

    if test "${input:+set}" != "set" ; then
      error "Parameter \"input\" in function \"extract_contents\" has not been set or has null value.\n" "2" >&2
    fi &&

    for content in "${input}/"?* ; do
      set -- "input=${content}" &&
      if test "${key:+set}" = "set" ; then
        set -- "${@}" "key=${key}"
      fi &&
      if test "${output:+set}" = "set" ; then
        set -- "${@}" "output=${output}"
      fi
      extract_single "${@}"
    done
  ) }

  pulse () { (
    amount="${1}"
    frequency="${2}"
    width="${3}"

    for _ in $(seq 1 "${amount}") ; do
      sleep "${frequency}" &&
      timeout "${width}" pacat < /dev/urandom
    done
  ) }

# argument parsing loop from https://stackoverflow.com/a/28466267
  while getopts achk:m:o:s:Vx-: argument; do
    case "${argument}" in
      a )
        boolean_add=0
      ;;
      c )
        boolean_process_contents=0
      ;;
      h )
        print_help >&2 &&
        will_exit=0
      ;;
      k )
        key="${OPTARG}"
      ;;
      m )
        method="${OPTARG}"
      ;;
      o )
        output="${OPTARG}"
      ;;
      s )
        split="${OPTARG}"
      ;;
      V )
        print_version "${file_self}" "${version_self}" >&2 &&
        will_exit=0
      ;;
      x )
        set -x
      ;;
      - )
        optarg_long="${OPTARG#*=}"
        case "${OPTARG}" in
          add )
            boolean_add=0
          ;;
          process-contents )
            boolean_process_contents=0
          ;;
          help )
            print_help >&2 &&
            will_exit=0
          ;;
          key=?* )
            key="${optarg_long}"
          ;;
          key )
            eval "key=\"\$${OPTIND}\"" &&
            if test -z "${key}" ; then
              error_arguments_empty "${OPTARG}" >&2
            fi &&
            OPTIND=$((OPTIND+1))
          ;;
          key* )
            error_arguments_empty "${OPTARG}" >&2
          ;;
          method=?* )
            method="${optarg_long}"
          ;;
          method )
            eval "method=\"\$${OPTIND}\"" &&
            if test -z "${method}" ; then
                error_arguments_empty "${OPTARG}" >&2
            fi &&
            OPTIND=$((OPTIND+1))
          ;;
          method* )
            error_arguments_empty "${OPTARG}" >&2
          ;;
          output=?* )
            output="${optarg_long}"
          ;;
          output )
            eval "output=\"\$${OPTIND}\"" &&
            if test -z "${output}" ; then
              error_arguments_empty "${OPTARG}" >&2
            fi &&
            OPTIND=$((OPTIND+1))
          ;;
          output* )
            error_arguments_empty "${OPTARG}" >&2
          ;;
          split=?* )
          split="${optarg_long}"
          ;;
          split )
          eval "split=\"\$${OPTIND}\"" &&
            if test -z "${split}" ; then
              error_arguments_empty "${OPTARG}" >&2
            fi &&
            OPTIND=$((OPTIND+1))
          ;;
          split* )
            error_arguments_empty "${OPTARG}" >&2
          ;;
          version )
            print_version "${file_self}" "${version_self}" >&2 &&
            will_exit=0
          ;;
          debug )
            set -x
          ;;
          add* | process-contents* | help* | version* | debug* )
            error_arguments_contained >&2
          ;;
#         "--" terminates argument processing
          '' )
            break
          ;;
          * )
            error_option_invalid >&2
          ;;
        esac
      ;;
#     getopts already reported the illegal option
      \? )
        exit 2
      ;;
    esac
  done &&
# remove parsed options and arguments from "${@}" list
  shift $((OPTIND-1)) &&

  if test "${will_exit}" -eq 0 ; then
    exit 0
  fi &&

# main
  if test "${1:+set}" = "set" ; then
#   arguments have been provided, parse them
    for argument in "${@}" ; do
#     check if adding or extracting
      if test "${boolean_add}" -eq 0 ; then
#       will add, now check if archiving a single file or contents of a directory individually
        if test -d "$(readlink --canonicalize "${argument}")" && test "${boolean_process_contents}" -eq 0; then
#         will archive directory contents individually
          add_contents "input=${argument}" "output=${output:+}" "key=${key:+}" "method=${method:+}" "split=${split:+}"
        else
          add_single "input=${argument}" "output=${output:+}" "key=${key:+}" "method=${method:+}" "split=${split:+}"
        fi
      else
#       will extract, now check if extracting a single file or contents of a directory individually
        if test -d "$(readlink --canonicalize "${argument}")" && test "${boolean_process_contents}" -eq 0 ; then
#         will extract directory contents individually
          extract_contents "input=${argument}" "key=${key:+}" "output=${output:+}"
        else
          extract_single "input=${argument}" "key=${key:+}" "output=${output:+}"
        fi
      fi
    done
  else
#   no arguments were provided, process contents of current director
    if test "${boolean_add}" -eq 0 ; then
      add_contents "input=${PWD}" "output=${output:+}" "key=${key:+}" "method=${method:+}"
    else
      extract_contents "input=${PWD}" "key=${key:+}" "output=${output:+}"
    fi
  fi &&
# todo: make this optional
  pulse 20 0.03 0.05 &&
  exit 0
) }

main "${@}"
