#!/bin/sh

# Dependencies: `lux`

main () {
  brightness="$(lux -G | tr -d '%')"

  if [ "${brightness}" -ge "75" ] ; then
    printf '%s/scripts/panels/images/brightness-100.svg' "${HOME}"
  elif [ "${brightness}" -ge "50" ] ; then
    printf '%s/scripts/panels/images/brightness-75.svg' "${HOME}"
  elif [ "${brightness}" -ge "25" ] ; then
    printf '%s/scripts/panels/images/brightness-50.svg' "${HOME}"
  else
    printf '%s/scripts/panels/images/brightness-25.svg' "${HOME}"
  fi
}

main
