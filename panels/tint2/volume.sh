#!/bin/sh

# Dependencies: `pulseaudio`, `awk`

get_default_sink_name() {
  pacmd stat | awk -F": " '/^Default sink name: /{print $2}'
}

get_default_sink_volume() {
  pacmd list-sinks |
#   find section about default sink, find section about volume, find 5th field (volume as %age), truncate last character (% symbol) to prevent errors later
    awk '/^\s+name: /{default_sink = $2 == "<'$(get_default_sink_name)'>"}
      /^\s+volume: / && default_sink {$5=substr($5,1,length($5)-1); print $5; exit}'
}

main () {
  printf '%s%%' "$(get_default_sink_volume)"
}

main
