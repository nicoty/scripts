#!/bin/sh

# Dependencies: `pulseaudio`, `awk`, `oomox`

get_default_sink_name() {
  pacmd stat | awk -F": " '/^Default sink name: /{print $2}'
}

get_default_sink_volume() {
  pacmd list-sinks |
#   find section about default sink, find section about volume, find 5th field (volume as %age), truncate last character (% symbol) to prevent errors later
    awk '/^\s+name: /{default_sink = $2 == "<'$(get_default_sink_name)'>"}
      /^\s+volume: / && default_sink {$5=substr($5,1,length($5)-1); print $5; exit}'
}

default_sink_muted () {
  pacmd list-sinks |
    awk '/^\s+name: /{default_sink = $2 == "<'$(get_default_sink_name)'>"}
      /^\s+muted: / && default_sink {print $2; exit}'
}

main () {
  case "$(default_sink_muted)" in
    *yes*)
    printf '%s/scripts/panels/images/volume-0.svg' "${HOME}"
    ;;

    *)
      volume=$(get_default_sink_volume)

      if [ ${volume} -ge 60 ] ; then
        printf '%s/scripts/panels/images/volume-100.svg' "${HOME}"
      elif [ ${volume} -ge 30 ] ; then
        printf '%s/scripts/panels/images/volume-67.svg' "${HOME}"
      else
        printf '%s/scripts/panels/images/volume-33.svg' "${HOME}"
      fi
    ;;
  esac
}

main
